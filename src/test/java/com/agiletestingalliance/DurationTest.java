package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class DurationTest {

	@Test
	public void testCPDOF(){

	AboutCPDOF about = new AboutCPDOF();
	assertTrue(about.desc().contains("certi"));

	}

	@Test
	public void testDuration() {

	Duration duration = new Duration();
	assertTrue(duration.dur().contains("specifically"));

	}	

	@Test
	public void testMinMax(){

	MinMax minMax = new MinMax();
	assertEquals(minMax.getMax(3,5),5);
	assertEquals(minMax.getMax(5,2),5);

	}

	@Test
	public void testUsefullness(){

	Usefulness use = new Usefulness();
	assertTrue(use.desc().contains("DevOps"));

	}
} 
